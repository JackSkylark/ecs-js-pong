import { Component } from "@bytebros/ecs";

export class BoxRenderable2D extends Component {
    constructor(width, height) {
        super();
        this.width = width;
        this.height = height;
        this.isEnabled = true;
    }
}
