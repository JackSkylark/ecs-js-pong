import { Component } from "@bytebros/ecs";

export class Movement2D extends Component {
    constructor(moveX, moveY, speed) {
        super();
        this.moveX = moveX;
        this.moveY = moveY;
        this.speed = speed;
    }
}
