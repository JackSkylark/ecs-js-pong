import { Component } from "@bytebros/ecs";

export class Position2D extends Component {
    constructor(x, y) {
        super();
        this.x = x;
        this.y = y;
    }
}
