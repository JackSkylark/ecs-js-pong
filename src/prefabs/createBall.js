import { Ball } from "../components/Ball";
import { Position2D } from "../components/Position2D";
import { BoxRenderable2D } from "../components/BoxRenderable2D";
import { Movement2D } from "../components/Movement2D";

export default (entityStore, canvas) =>
{
    return entityStore
        .createEntity()
        .addComponents(
            new Ball(),
            new Position2D((canvas.width / 2) - 9, (canvas.height / 2) - 9),
            new BoxRenderable2D(18, 18),
            new Movement2D(0, 0, 0.3)
        );
}
