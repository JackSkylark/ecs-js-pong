import { ECS, EntityStore } from "@bytebros/ecs";
import { BoxRenderable2D } from "./components/BoxRenderable2D";
import { Position2D } from "./components/Position2D";
import { Movement2D } from "./components/Movement2D";
import { RenderingSystem } from "./systems/renderingSystem";
import { MovementSystem } from "./systems/movementSystem";
import { Player1 } from "./components/Player1";
import { Player2 } from "./components/Player2";
import { Ball } from "./components/Ball";
import { Player1InputSystem } from "./systems/player1InputSystem";
import { Player2InputSystem } from "./systems/player2InputSystem";
import { BallServeSystem } from "./systems/ballServeSystem";
import createBall from "./prefabs/createBall";
import { CANVAS_WIDTH, CANVAS_HEIGHT } from "./constants";
import { BallWallCollisionSystem } from "./systems/ballWallCollisionSystem";
import { Collidable } from "./components/Collidable";
import { Collider2D } from "./components/Collider2D";
import { GameState } from "./gameState";

var DIRECTION = {
	IDLE: 0,
	UP: 1,
	DOWN: 2,
	LEFT: 3,
	RIGHT: 4
};

function main()
{
    var canvas = document.querySelector("canvas");
    canvas.width = CANVAS_WIDTH;
    canvas.height = CANVAS_HEIGHT;
    canvas.style.width = (canvas.width / 2) + 'px';
    canvas.style.height = (canvas.height / 2) + 'px';

    var entityStore = new EntityStore();

    var ball = createBall(entityStore, canvas);

    var gameState = new GameState();

    var leftPaddle = 
        entityStore
            .createEntity()
            .addComponents(
                new Player1(),
                new Position2D(150, (CANVAS_HEIGHT / 2) - 35),
                new BoxRenderable2D(18, 70),
                new Movement2D(0, 0, 0.7),
                new Collidable(),
                new Collider2D({x: 18, y: 70})
            );

    var rightPaddle =
        entityStore
            .createEntity()
            .addComponents(
                new Player2(),
                new Position2D((CANVAS_WIDTH - 150), (CANVAS_HEIGHT / 2) - 35),
                new BoxRenderable2D(18, 70),
                new Movement2D(0, 0, 0.7),
                new Collidable(),
                new Collider2D({x: 18, y: 70})
            );

    var renderingSystem = new RenderingSystem(canvas, gameState);

    var renderECS = new ECS(entityStore);
    renderECS.addSystem(renderingSystem);

    var gameECS = new ECS(entityStore);
    gameECS
        .addSystem(new Player1InputSystem())
        .addSystem(new Player2InputSystem())
        .addSystem(new MovementSystem())
        .addSystem(new BallServeSystem())
        .addSystem(new BallWallCollisionSystem(canvas, entityStore, gameState));

    function loop(timestamp)
    {
        var dt = timestamp - lastRender;
        lastRender = timestamp;
        gameECS.updateSystems(dt);
        renderECS.updateSystems(dt);

        requestAnimationFrame(loop);
    }

    var lastRender = performance.now();
    window.requestAnimationFrame(loop);
}

document.addEventListener('DOMContentLoaded', (event) => {
    main();
})
