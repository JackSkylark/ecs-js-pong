import { System } from "@bytebros/ecs";
import { Position2D } from "../components/Position2D";
import { BoxRenderable2D } from "../components/BoxRenderable2D";

export class RenderingSystem extends System
{
    constructor(
        canvas,
        gameState
    )
    {
        super();
        this.color = "#2c3e50";
        this.canvas = canvas;
        this.context = canvas.getContext("2d");
        this.gameState = gameState;
        this.query = [
            Position2D,
            BoxRenderable2D
        ];
    }

    run(dt, entities)
    {
        this.clearCanvas();
        this.context.fillStyle = "#ffffff";

        for (let i = 0; i < entities.length; i++) {
            const entity = entities[i];
            const position = entity.getComponent(Position2D);
            const box = entity.getComponent(BoxRenderable2D);

            if (!box.isEnabled)
            {
                continue;
            }

            this.context.fillRect(
                position.x,
                position.y,
                box.width,
                box.height
            );
        }

        // Draw the net (Line in the middle)
		this.context.beginPath();
		this.context.setLineDash([7, 15]);
		this.context.moveTo((this.canvas.width / 2), this.canvas.height - 30);
		this.context.lineTo((this.canvas.width / 2), 30);
		this.context.lineWidth = 5;
		this.context.strokeStyle = '#ffffff';
		this.context.stroke();

        // Set the default canvas font and align it to the center
        this.context.font = '100px Courier New';
        this.context.textAlign = 'center';

        // Draw the players score (left)
        this.context.fillText(
            this.gameState.player1Score.toString(),
            (this.canvas.width / 2) - 300,
            200
        );

        // Draw the paddles score (right)
        this.context.fillText(
            this.gameState.player2Score.toString(),
            (this.canvas.width / 2) + 300,
            200
        );
    }

    clearCanvas()
    {
        this.context.clearRect(
            0,
            0,
            this.canvas.width,
            this.canvas.height
        );

        // Set the fill style to black
		this.context.fillStyle = this.color;

		// Draw the background
		this.context.fillRect(
			0,
			0,
			this.canvas.width,
			this.canvas.height
		);
    }


}