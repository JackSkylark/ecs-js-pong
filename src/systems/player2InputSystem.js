import { System } from "@bytebros/ecs";
import { Movement2D } from "../components/Movement2D";
import { Player2 } from "../components/Player2";

export class Player2InputSystem extends System
{
    constructor()
    {
        super();
        this.keys = {
            up: false,
            down: false
        };
        this.query = [
            Player2,
            Movement2D
        ];

        document.addEventListener("keydown", key =>
        {
            if (key.keyCode === 87)
            {
                this.keys.up = true;
            }

            if (key.keyCode === 83)
            {
                this.keys.down = true;
            }
        });

        document.addEventListener("keyup", key =>
        {
            if (key.keyCode === 87)
            {
                this.keys.up = false;
            }

            if (key.keyCode === 83)
            {
                this.keys.down = false;
            }
        });
    }

    run(dt, entities)
    {
        for (let i = 0; i < entities.length; i++) {
            const entity = entities[i];
            const movement = entity.getComponent(Movement2D);

            if (this.keys.up)
            {
                movement.moveY = -1;
            }
            else if (this.keys.down)
            {
                movement.moveY = 1;
            }
            else
            {
                movement.moveY = 0;
            }
        }
    }
}