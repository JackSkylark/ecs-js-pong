import { System } from "@bytebros/ecs";
import { Position2D } from "../components/Position2D";
import { Movement2D } from "../components/Movement2D";

export class MovementSystem extends System
{
    constructor()
    {
        super();
        this.query = [
            Position2D,
            Movement2D
        ];
    }

    run(dt, entities)
    {
        for (let i = 0; i < entities.length; i++) {
            const entity = entities[i];
            const movement = entity.getComponent(Movement2D);
            const position = entity.getComponent(Position2D);

            if (movement.moveX != 0)
            {
                position.x += (movement.moveX * movement.speed * dt);
            }

            if (movement.moveY != 0)
            {
                position.y += (movement.moveY * movement.speed * dt);
            }
        }
    }
}
