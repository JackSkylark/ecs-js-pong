import { System } from "@bytebros/ecs";
import { Ball } from "../components/Ball";
import { Movement2D } from "../components/Movement2D";

export class BallServeSystem extends System
{
    constructor()
    {
        super();
        this.query = [
            Ball,
            Movement2D
        ];
    }

    run(dt, entities)
    {
        for (let i = 0; i < entities.length; i++) {
            const entity = entities[i];
            const ball = entity.getComponent(Ball);
            const movement = entity.getComponent(Movement2D);

            if (ball.hasServed)
            {
                continue;
            }

            movement.moveX = [-1, 1][Math.round(Math.random())];
            movement.moveY = [-1, 1][Math.round(Math.random())];
            ball.hasServed = true;
        }
    }
}
