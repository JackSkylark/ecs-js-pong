import { System } from "@bytebros/ecs";
import { Ball } from "../components/Ball";
import { Movement2D } from "../components/Movement2D";
import { Position2D } from "../components/Position2D";
import createBall from "../prefabs/createBall";
import { Collidable } from "../components/Collidable";

export class BallWallCollisionSystem extends System
{
    constructor(
        canvas,
        entityStore,
        gameState
    )
    {
        super();
        this.canvas = canvas;
        this.entityStore = entityStore;
        this.gameState = gameState;
        this.query = [
            Ball,
            Movement2D
        ];
    }

    run(dt, entities)
    {
        for (let i = 0; i < entities.length; i++) {
            const entity = entities[i];
            const movement = entity.getComponent(Movement2D);
            const position = entity.getComponent(Position2D);

            if (position.x <= 0)
            {
                // score
                this.gameState.player2Score += 1;
                entity.destroy();
                createBall(this.entityStore, this.canvas);
            }
            else if (position.x >= this.canvas.width)
            {
                //score
                this.gameState.player1Score += 1;
                entity.destroy();
                createBall(this.entityStore, this.canvas);
            }

            if (position.y <= 0)
            {
                movement.moveY = 1;
            }
            else if (position.y >= this.canvas.height)
            {
                movement.moveY = -1;
            }

            if (this.isColliding(position))
            {
                movement.moveY = [-1, 1][Math.round(Math.random())];
                movement.moveX = -movement.moveX;
            }
        }
    }

    isColliding(
        ballPosition
    )
    {
        const margin = 30;
        var collidableEntities = this.entityStore.query(Collidable, Position2D);
        for (let i = 0; i < collidableEntities.length; i++) {
            const entity = collidableEntities[i];
            const entityPosition = entity.getComponent(Position2D);
            
            if (ballPosition.x <= entityPosition.x + margin && ballPosition.x >= entityPosition.x - margin)
            {
                if (ballPosition.y <= entityPosition.y + margin && ballPosition.y >= entityPosition.y - margin)
                {
                    return true;
                }
            }
        }

        return false;
    }
}
